package me.arnoldsk.magicmirror;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.Recipe;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;

public class ItemData {
    MagicMirror plugin;

    String itemKey = "magic_mirror";
    String name = ChatColor.AQUA + "Magic Mirror";
    String description = "Gaze in the mirror to return home";
    Material material = Material.MUSIC_DISC_WAIT;

    ItemStack item;

    ItemData(MagicMirror plugin) {
        this.plugin = plugin;

        // Create a new custom item
        item = new ItemStack(material);

        // Adjust metadata
        ItemMeta meta = item.getItemMeta();

        if (meta != null) {
            // Name
            meta.setDisplayName(name);

            // Lore
            ArrayList<String> lore = new ArrayList<>();
            lore.add(description);
            meta.setLore(lore);

            // Flags - Hide everything
            meta.addItemFlags(
                ItemFlag.HIDE_ENCHANTS,
                ItemFlag.HIDE_ATTRIBUTES,
                ItemFlag.HIDE_UNBREAKABLE,
                ItemFlag.HIDE_DESTROYS,
                ItemFlag.HIDE_PLACED_ON,
                ItemFlag.HIDE_POTION_EFFECTS
            );

            item.setItemMeta(meta);
        }
    }

    public Recipe getRecipe() {
        NamespacedKey key = new NamespacedKey(plugin, itemKey);
        ShapedRecipe recipe = new ShapedRecipe(key, item);

        // Set the recipe shape
        recipe.shape(
            "IEI",
            "EDE",
            "IEI"
        );

        // Map the shape items
        recipe.setIngredient('I', Material.IRON_INGOT);
        recipe.setIngredient('E', Material.ENDER_EYE);
        recipe.setIngredient('D', Material.DIAMOND);

        return recipe;
    }
}
