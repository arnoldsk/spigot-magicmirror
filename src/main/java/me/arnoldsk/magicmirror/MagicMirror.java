package me.arnoldsk.magicmirror;

import org.bukkit.plugin.java.JavaPlugin;

public final class MagicMirror extends JavaPlugin {
    @Override
    public void onEnable() {
        // Add listeners
        getServer().getPluginManager().registerEvents(new GazeListener(), this);

        // Get the custom item data
        ItemData itemData = new ItemData(this);

        // Add the custom item recipe to server
        getServer().addRecipe(itemData.getRecipe());
    }
}
