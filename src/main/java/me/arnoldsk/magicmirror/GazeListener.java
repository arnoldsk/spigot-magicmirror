package me.arnoldsk.magicmirror;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

public class GazeListener implements Listener {
    void TeleportPlayerHome(Player player) throws InterruptedException {
        Location bedSpawnLocation = player.getBedSpawnLocation();

        // Require a bed
        if (bedSpawnLocation == null) {
            player.sendMessage("You do not have a bed spawn set");
            return;
        }

        // Require being on ground
        if (!player.isOnGround()) {
            player.sendMessage("You must be on the ground to teleport");
            return;
        }

        // Require sneaking
        if (!player.isSneaking()) {
            player.sendMessage("You must be sneaking to teleport");
            return;
        }

        // If player is not there already
        if (bedSpawnLocation.getBlock().equals(player.getLocation().getBlock())) {
            return;
        }

        // Display particles at the player position before teleporting
        TeleportParticles(player);

        // Play a sound effect
        player.playSound(player.getLocation(), Sound.BLOCK_BEACON_ACTIVATE, (float) 0.5, 2);

        // Delay for a while
        TimeUnit.SECONDS.sleep(1);

        player.sendMessage(ChatColor.AQUA + "Whoosh...");
        player.teleport(bedSpawnLocation, PlayerTeleportEvent.TeleportCause.PLUGIN);

        // Display particles at the destination after teleporting
        TeleportParticles(player);
    }

    void TeleportParticles(Player player) {
        Location location = player.getLocation();
        int count = 50;
        double offsetX = 1;
        double offsetY = 2;
        double offsetZ = 1;

        player.spawnParticle(Particle.SPELL_INSTANT, location, count, offsetX, offsetY, offsetZ);
        player.spawnParticle(Particle.SPELL_WITCH, location, count, offsetX, offsetY, offsetZ);
    }

    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent event) throws InterruptedException {
        Player player = event.getPlayer();
        Action action = event.getAction();

        // The action has to be a right click
        if (!(action == Action.RIGHT_CLICK_AIR || action == Action.RIGHT_CLICK_BLOCK)) {
            return;
        }

        // The item has to be the magic mirror
        PlayerInventory inventory = player.getInventory();
        ItemMeta meta = inventory.getItemInMainHand().getItemMeta();

        if (meta == null || !meta.hasLore()) {
            return;
        }

        ArrayList<String> lore = (ArrayList<String>) meta.getLore();

        if (lore == null || !lore.get(0).equals("Gaze in the mirror to return home")) {
            return;
        }

        // Finally, teleport the player to bed
        TeleportPlayerHome(player);
    }
}